FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive

# Install systemd and necessary packages
RUN apt-get update && \
    apt-get install -y systemd expect wget nano && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Remove unnecessary systemd services
RUN systemctl mask \
    dev-hugepages.mount \
    sys-fs-fuse-connections.mount \
    sys-kernel-config.mount \
    sys-kernel-debug.mount \
    sys-fs-fuse-connections.mount \
    display-manager.service \
    getty@.service \
    systemd-logind.service \
    systemd-remount-fs.service \
    systemd-udevd.service \
    systemd-update-utmp.service \
    systemd-user-sessions.service

RUN apt-get update && apt-get upgrade -y

COPY instal-zabbix-server.sh /home/instal-zabbix-server.sh
RUN chmod +x /home/instal-zabbix-server.sh 
    #&& /tmp/instal-zabbix-server.sh

EXPOSE 80 443 3306 10050 10051

# Enable systemd init system
CMD ["/lib/systemd/systemd"]