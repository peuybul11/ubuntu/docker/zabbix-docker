sudo apt update -y
sudo apt upgrade -y

# install apache2
sudo apt -y install apache2

# install php
sudo apt -y install php php-cgi libapache2-mod-php php-common php-pear php-mbstring
sudo sed -i 's/;date.timezone =/date.timezone = Asia\/Jakarta/' /etc/php/7.4/apache2/php.ini
echo '<?php phpinfo(); ?>' | sudo tee /var/www/html/info.php
sudo systemctl restart apache2

# install mariadb
sudo apt -y install mariadb-server
sudo sed -i 's/^# *\(character-set-server *= *\).*/\1utf8mb4/' /etc/mysql/mariadb.conf.d/50-server.cnf
sudo sed -i 's/^# *\(collation-server *= *\).*/\1utf8mb4_unicode_ci/' /etc/mysql/mariadb.conf.d/50-server.cnf
sudo sed -i 's/^bind-address.*$/bind-address = 0.0.0.0/' /etc/mysql/mariadb.conf.d/50-server.cnf

sudo systemctl restart mariadb

MYSQL_PASSWORD=zabbix
sudo mysql_secure_installation <<EOF
Y
$MYSQL_PASSWORD
N
Y
Y
Y
Y
EOF

# install zabbix
wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb
sudo dpkg -i zabbix-release_5.0-1+focal_all.deb
sudo apt update -y
sudo apt upgrade -y
sudo apt -y install zabbix-server-mysql zabbix-apache-conf zabbix-agent zabbix-frontend-php php-mysql php-gd php-bcmath php-net-socket php-pear

# create database & user
sudo mysql <<EOF
create database zabbix character set utf8 collate utf8_bin;
grant all privileges on zabbix.* to zabbix@'%' identified by 'zabbix';
flush privileges;
exit
EOF

# update package
sudo apt update && sudo apt upgrade -y

# restore db
cd /usr/share/doc/zabbix-server-mysql
sudo gzip -d create.sql.gz
sudo mysql zabbix < create.sql

sudo sed -i 's/^#\? *\(DBName *= *\).*/\1zabbix/' /etc/zabbix/zabbix_server.conf
sudo sed -i 's/^#\? *\(DBUser *= *\).*/\1zabbix/' /etc/zabbix/zabbix_server.conf
sudo sed -i 's/^#\? *\(DBPassword *= *\).*/\1zabbix/' /etc/zabbix/zabbix_server.conf

sudo systemctl restart zabbix-server

sudo sed -i 's/Allow from 0.0.0.0\/0/Allow from 192.168.1.0\/24/g' /etc/apache2/conf-enabled/zabbix.conf
sudo sed -i 's/^\s*php_value\s+date.timezone\s\+Asia\/Tokyo/php_value date.timezone Asia\/Jakarta/g' /etc/apache2/conf-enabled/zabbix.conf

sudo systemctl restart apache2
